{\rtf1\ansi\ansicpg936\cocoartf1504\cocoasubrtf830
{\fonttbl\f0\fnil\fcharset0 Menlo-Regular;}
{\colortbl;\red255\green255\blue255;\red100\green56\blue32;\red0\green0\blue0;}
{\*\expandedcolortbl;;\csgenericrgb\c39100\c22000\c12500;\csgenericrgb\c0\c0\c0;}
\paperw11900\paperh16840\margl1440\margr1440\vieww17640\viewh13720\viewkind0
\deftab543
\pard\tx543\pardeftab543\pardirnatural\partightenfactor0

\f0\fs22 \cf2 \CocoaLigature0 ## 1.	Abstract (use 3 to 5 sentences to introduce the core idea of the article):\
\cf3 \
The article introduces the index constituent stocks. Using the time window method, the author found the change of index stocks before and after announcing the list. By using long/short strategy which can avoid impact of other factors, as a result, gain positive profit.\
\cf2 \
## 2.	What problem does the article solve?\cf3 \
\
The article observed the change of HS300 before and after announcing the list, and used long/short strategy to find the opportunity of invest which can gain much more positive income.\
\cf2 \
## 3.	How does the article solve the problem?\cf3 \
\
First, the article gain the daily earnings per stocks by using API. Second, calculate excess income.Then use the time window method to observe the trend of index stocks. Next, use long/short strategy to gain profit during the position cycle.\
\cf2 \
## 4.	Impact (why the idea in article is so important and deserved to be published)?\
\cf3 \
The adjustment of index stock portfolio can influence each stock. Since HS300 adjust the index stocks regularly, we can find the opportunity of investment by calculating the stock weight and scale to earn more profits. Moreover, acknowledge the exact time to buy stocks can guarantee the percentage of positive income.\cf2 \
\cf3 \
\
\cf2 ## 5.	Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)\cf3 \
\
Yes, the scale of the problem is appropriate since the number of HS300 is not too large. And the method which called long/short strategy is not too complicated to understand. \
\cf2 \
## 6.	Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).\cf3 \
\
Yes, the long/short strategy and time window method are implementable and not too complex. Those two method are well defined and the API which is mentioned at the beginning of article can be finished by computer.\
\
\cf2 ## 7.	Is there any possible way to improve the solution in article?\cf3 \
\
In my opinion, the solution can be improved. For example, we can calculate the exact time to long or short the stocks which can guarantee the percentage of positive income. As a result, we can find a way to invest.\
\cf2 \
## 8.	Can you collect the data to complete the experiment? (sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data)\cf3 \
\
The data of HS300 can be collected on the internet such as eastmoney.com.}