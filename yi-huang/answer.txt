1.Abstract (use 3 to 5 sentences to introduce the core idea of the article):
This is an article about index constituent Alpha enhancement. The content of this article is demonstrating a method which is constructed by CSI300 Alpha enhancement and CSI500 Alpha enhancement.
2.What problem does the article solve?
This analysis will reduce volatility and maximum retracement. Moreover, the new abnormal returns have been created.
3.How does the article solve the problem?
The principle of exponential enhancement is to remove the part that generates negative excess returns and to assign greater weight to constituent components of positive excess returns.
4.Impact (why the idea in article is so important and deserved to be published)?
When you find these Alpha enhanced stocks and investment these products, not only you can get the better support than the long term investment index funds, but also you can gain abnormal returns.
5.Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)
Yes,according to this paper, it applied analysis the Alpha to observe earnings.
6.Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).
Yes,we can use multiple methods to build a portfolio of constituent Alpha. And to find out more factors which might affect the Alpha. 
7.Is there any possible way to improve the solution in article?
Yes, stock selection will be more appropriate if you choose the 300 equal rights income index and the 500 equal rights income index.
8.Can you collect the data to complete the experiment? (Sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data).
Yes, it is easy to collect this data from some websites,such as Yahoo Finance and Google Finance.