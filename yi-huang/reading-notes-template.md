## 1.	Abstract (use 3 to 5 sentences to introduce the core idea of the article):



## 2.	What problem does the article solve?



## 3.	How does the article solve the problem?



## 4.	Impact (why the idea in article is so important and deserved to be published)?



## 5.	Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)



## 6.	Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).



## 7.	Is there any possible way to improve the solution in article?



## 8.	Can you collect the data to complete the experiment? (sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data)



