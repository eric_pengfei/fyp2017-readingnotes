## 1.	Abstract (use 3 to 5 sentences to introduce the core idea of the article):

This paper use Moving Average Convergence Divergence(MACD)indicator technical analysis tool to identify trading signals based on the trends in stock prices.They use standard and refined MACD indicators on Indian Stock Markets during 1997-2010,and test returns of MACD Indicator.Then,this paper find the return of refined MACD indicators outperformed the benchmark market returns.Thus, this paper challenge the Efficient Market Hypothesis, which rules out the possibility of earning excess returns.

## 2.	What problem does the article solve?

This articles not only measures the profitability of MACD rules,but also test the effiacy of MACD rules over market return and the impact of brokerage cost on the returns of the trading rules tested.

## 3.	How does the article solve the problem?

First, the paper gives how to identify MACD indicitors that the MACD indicates the buy and sell signal based on convergence and divergence of two different moving averages of the stock prices.To caculate buy signal, it is given by Exponential Moving Average(EMA) crosses the loner moving average from below. To caculate sell signal, it is given by shorter moving average crosses the longer moving average from above.Use these two priniciples,the paper can get buy and sell signals. The profitability of MACD rule is studied on the movment of Sensex and Nifty.Using stocks in these two trading areas in Indian Stock Exchange, the author test the profitability of different MACD rules. 


## 4.	Impact (why the idea in article is so important and deserved to be published)?

This article gives an idea that using MACD model to select stocks and applied it to real India market to test the rule.This method applies theory to practice.

## 5.	Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)

The scale of the problem is small enough to be applied, since it just measures MACD indicatiors and applied it in Sensex and Nifty.

## 6.	Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).

The solution is implementable. It gives specific data to show the profitabiliry of MACD indicatiors and compare with hold returns.

## 7.	Is there any possible way to improve the solution in article?

The article introuduce the profitability of MACD rules,and test the effiacy of MACD rules over market return. But the author do not give any theoretical proof for efficient market hypthoesis,in our article, we can wipe out this part since it is not necessary for support the measures of MACD indicatiors,

## 8.	Can you collect the data to complete the experiment? (sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data)

Yes, we can get stocks from Shanghai Stock Exchange and Shenzhen Stock Exchange in China stock market. Applied MACD rules into chinese stock markets to choose stocks and test it.

