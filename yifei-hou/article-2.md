## 1.	Abstract (use 3 to 5 sentences to introduce the core idea of the article):
	
This article studies the Momentum effect on A shares market. It compares two Momentum strategies, fixed holding period momentum strategy (general strategy) and random holding period momentum strategy (new strategy). From these two strategies, there is significant momentum effect in the new strategy, while there is no marked momentum effect in general strategy.

## 2.	What problem does the article solve?

This article provides a new momentum strategy to do the quantity investment. 

## 3.	How does the article solve the problem?

This article uses two strategies to measure the momentum effect of A shares. And then compares these two methods, and finds the new method is meaningful.

## 4.	Impact (why the idea in article is so important and deserved to be published)?

If the new momentum strategy was verified, there would be a new strategy for people to do the quantity investment. And then, people maybe create other methods on the basis of this momentum strategy. 

## 5.	Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)

Yes, we can uses this new strategy to verify other stocks, and get a return.

## 6.	Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).

Maybe the new momentum strategy should do more verification. 

## 7.	Is there any possible way to improve the solution in article?

New momentum strategy has not been verified yet. We should measure momentum effect of other stocks by applying this new strategy and doing more exercise.

## 8.	Can you collect the data to complete the experiment? (sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data)

Yes.


