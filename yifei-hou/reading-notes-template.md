## 1.	Abstract (use 3 to 5 sentences to introduce the core idea of the article):
	
This paper has two aspects, one is that it introduces GARP stock-selecting model, and the other is that it finds the timing signal through establishing Markov Chain forecasting model. This paper also verifys these two model by studying Shanghai Composite Index as a pool of stocks.


## 2.	What problem does the article solve?

The article provides a new idea of combining GARP model and Markov Chain model for quantitative investment.

## 3.	How does the article solve the problem?

This artical first selects some values of company such as PE, PB, PS, PCF, ROE and etc. And then sets up GARP model, next uses Maokov Chain model to find timing signal based on GARP midel.

## 4.	Impact (why the idea in article is so important and deserved to be published)?

It provides a new idea for quantiative investment. People may get a stable profit and enable to control and manage the risk by combining GARP model and Markov Chain.

## 5.	Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)

Yes, it can be applied into FYP, according to this paper, it applied MATLAB as its software.

## 6.	Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).

Yes, this artical takes Shanghai Composite Index for an example.

## 7.	Is there any possible way to improve the solution in article?

Yes, the factors selected should be verify its effectiveness, it should be considered based on the real return of investment. So, the GARP model may be established based on different factors due to different investment, this artical find the GARP model of shanghai Composite Index, when applied another index as stock pool, it is important to verify this factors again and then select several suitable factors. 

## 8.	Can you collect the data to complete the experiment? (sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data)

Yes, it is easy to collect this data from some websites, such as Yahoo Finance and  Google Finance.

