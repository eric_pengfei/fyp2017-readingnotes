## 1.	Abstract (use 3 to 5 sentences to introduce the core idea of the article):

The article tries to investigate the correlation between the change of currency and change of gold price, and applies this correlation to trade gold.

## 2.	What problem does the article solve?

Generally, the article tries to design experiment to find the correlation between a bucket of currencies and gold price. Some currencies have relative strong correlation with gold while others are relative weak. Besides, at different times, the correlation pattern might differ. Thus, the central problem is to analyse when and what group of currencies are the highest indicators of future gold price.

## 3.	How does the article solve the problem?

The article tries to find the correlation by either visualizing the currency change and gold price change, or using formula to obtain the numerical correlation value.


## 4.	Impact (why the idea in article is so important and deserved to be published)?

Finding indicator of gold price is very important since it can directly be applied into trading strategy which is widely interested by different parties.

## 5.	Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)

The scale of the problem is appropriate since the number of major currencies is not large, and the correlation calculation method is fixed. Besides, the problem is well defined. In summary, the scale of the project is manageable.

## 6.	Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).

The solution is implementable since the method of calculating correlation is well defined.

## 7.	Is there any possible way to improve the solution in article?

The article views the change of currency as the only indicator of change of gold price, and uses this correlation in trading. Actually, we can try different other indicators, e.g. trends of major economic entities’ GDP trends, oil prices, or other factors, and discuss the correlation of those factors with gold price.

## 8.	Can you collect the data to complete the experiment? (sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data)

Currency data and gold price data can be collected from various resources since they are public economic indices.
