## 1.	Abstract (use 3 to 5 sentences to introduce the core idea of the article):

The article introduces an improved pairs trading strategy for two China's stocks. First, it uses co-integration to implement pairs trading, and then improves the strategy to achieve the characteristic of China's stock market which is called brick removal strategy. The back-test shows that the strategy is better than individual stock.


## 2.	What problem does the article solve?

The article tries to build a strategy that find two price highly correlated stocks. The central problem is whether the return of strategy will better than individual stock.


## 3.	How does the article solve the problem?

The article selects two highly price correlated stocks by using co-integration relationship. Then using OLS linear regression to calculate the coefficient of stationary series and fit with data. Besides, the article uses z-score to determine when to buy stocks


## 4.	Impact (why the idea in article is so important and deserved to be published)?

Establishing a pairs trading strategy is important, speculators can apply the strategy in practice to make a portfolio, which can reduce increase return on investment compare with individual stock.


## 5.	Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)

The scale of the problem is appropriate because the raw data is easy to access, and the method of the strategy is fixed and well defined. 


## 6.	Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).

The solution is implementable. The methods that select two highly price correlated stock are clearly defined and compute z-score is easy.


## 7.	Is there any possible way to improve the solution in article?
The article selects 120 days as test day and we can choose different test days to optimize the strategy. Besides, if we want to earn a steady return, we need to combine other strategy to analyze and judge the price trend of stocks, such as fundamental analysis.


## 8.	Can you collect the data to complete the experiment? (sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data)

The data of China’s stocks can be collected easily from different resources like Yahoo Finance.


