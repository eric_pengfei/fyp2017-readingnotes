## 1.	Abstract (use 3 to 5 sentences to introduce the core idea of the article):

The article constructs an improved trend tracking strategy for China’s CSI 300 Index Futures. The wavelet transform is used to denoise the price series, and then use the series and improved RSI to establish the model and test the strategy. The back-test shows that the strategy delivers positive returns and is applicable in practice. 


## 2.	What problem does the article solve?

The article tries to establish a strategy to track the trend effectively. The central problem is to determine when to entry and exit the market and whether to stop loss.  



## 3.	How does the article solve the problem?

The article selects the Relative Strength Index (RSI) as short-term indicators and improves it so that the trend of the market can be tracked effectively, and then applies it to open positions judgment. Besides, the article uses the wavelet transform to denoise the raw data, and then determine whether to stop loss.


## 4.	Impact (why the idea in article is so important and deserved to be published)?

Establishing a trend-tracking trading model is important, speculators can apply the strategy in practice to forecast the future trend of stock index futures prices, reduce investment risk and increase return on investment.


## 5.	Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)

The scale of the problem is appropriate because the raw data is easy to access, and the two methods of analyzing data are clearly defined. Moreover, the basic idea of strategy is concise and clear --- tracking the trend.


## 6.	Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).

The solution is implementable. The two methods of analyzing data are clearly defined and the entry and exit strategy can be recognized by computers.


## 7.	Is there any possible way to improve the solution in article?

The article uses improved RSI and wavelet transform to analyze the price series of CSI 300 Index Futures, and then track the trend. We can try other futures to establish trend-tracking trading strategy, e.g. the gold futures. Or we can use different methods to denoise the data, although the wavelet transform can reduce noise and retain characteristics, the future information and problems in processing the current time data cannot be accessible.


## 8.	Can you collect the data to complete the experiment? (sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data)

The data of China’s CSI 300 Index Futures can be collected easily from different resources like Yahoo Finance.


