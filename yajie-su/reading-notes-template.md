## 1.	Abstract (use 3 to 5 sentences to introduce the core idea of the article):
In this paper, the five-minute high-frequency transaction data of Shanghai Gold Phase 1606th contract is used to establish a practical and feasible timing and quantification trading system. The four single index strategies of MA, MACD, KDJ and RST in trend tracking strategy are studied, then the four index strategies are combined to be analyzed, and the yield of the portfolio is increased to 132%.



## 2.	What problem does the article solve?
The article tries to design a practical and feasible timing and quantification trading system to gain a higher return on gold futures.



## 3.	How does the article solve the problem?
Combining the four important single index strategies of MA, MACD, KDJ and RST in trend tracking strategy. 



## 4.	Impact (why the idea in article is so important and deserved to be published)?
In the post-crisis era, gold's hedging function can effectively evade the systemic risk in financial market and become the last barrier of fund security.


## 5.	Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)
The scale of the problem is appropriate because this paper is focus on gold future and the single index strategies is fixed.


## 6.	Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).
The solution is implementable because the single index strategies can be find and combine them to find out an optimal solution. And this paper use matlab to program.



## 7.	Is there any possible way to improve the solution in article?
The trading strategy introduced by this paper only combine four single index strategies to make trading decision. We can try to analyze the other single index strategies and combine them to find is better or not, such as DMI、EMA and VPT. 


## 8.	Can you collect the data to complete the experiment? (sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data)
The data can be found in some websites, such as Google finance.



