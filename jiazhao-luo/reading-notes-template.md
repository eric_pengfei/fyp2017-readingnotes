## 1.	Abstract (use 3 to 5 sentences to introduce the core idea of the article):
		Bitcoin becomes a perfect option to manage asset passively in long term investment. In this article, it
		compare bitcon to the other financial assets to figure out the correlation between them. Futhermore, 
		we can quantitatively define the investment holdings based on the other mature financial asset. Bitcoin 
		holds the characteristic of decentralization which enable it to hedge the negative political event.


## 2.	What problem does the article solve?
		This article figure out the lack of correlation between bitcoin and other financial assets so as to anayze
		its role and value on investment markets.


## 3.	How does the article solve the problem?
		The author adopted the Brute Force Approach to test the correlation between bitcoins and other financial
		assets firstly. Then, he took a optimazing plan to convince of his perspective.


## 4.	Impact (why the idea in article is so important and deserved to be published)?
		Bitcoin remains a special financial assets. If its potential value can be discovered in financial area, 
		it benefit most of the investment strategy at the present.
		


## 5.	Is the scale of the problem small enough to be applied into your FYP? (Sometimes the problem is too complex, deep, or broad, which is not appropriate to be chosen as FYP topic. Do not choose that kind of topic.)
		Yes, I think that will be included in my FYP.


## 6.	Is the solution in article implementable? (Sometimes the solution is too complex, broad, or deep, which is infeasible for you to do. Do not choose that kind of topic).
		The solution in article can be deepen which enable to carry on a further study,


## 7.	Is there any possible way to improve the solution in article?
		I think that about the correlation, we need to apply more kind of test to figure any correlation among them.
		We can also develop a strategy based on the result.


## 8.	Can you collect the data to complete the experiment? (sometimes we do not have data to test the method of solving the problem. Do not choose the topic you cannot collect data)
		I guess I can.



